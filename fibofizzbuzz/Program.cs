﻿//fibonacci+fizzbuzz laurai
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Numerics; // noretusi didesniu skaiciu. https://msdn.microsoft.com/en-us/library/system.numerics.biginteger(v=vs.110).aspx

namespace ConsoleApp4
{
    class Program
    {
        static void fizzbuzz(ulong x)
        {
            if ((x % 3 == x) && (x % 5 == 0))
            {
                Console.WriteLine("fizzbuzz");
            }
            else if (x % 3 == 0)
            {
                Console.WriteLine("fizz");
            }
            else if (x % 5 == 0)
            {
                Console.WriteLine("buzz");
            }
            else
            {
                Console.WriteLine(x);
            }
        }
        static void Main(string[] args)
        {
            ulong a = 0, b = 1;
            int limit = 21;
            Console.WriteLine(b);
            //while ((a <= limit) && (b <= limit)) {
            for (int i = 0; i < limit; i++)
            {
                a += b;
                fizzbuzz(a);
                b += a;
                fizzbuzz(b);
            }
        }
    }
}
